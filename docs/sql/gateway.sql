/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : open-platform

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2018-10-23 15:53:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gateway_rate_limit
-- ----------------------------
DROP TABLE IF EXISTS `gateway_rate_limit`;
CREATE TABLE `gateway_rate_limit` (
  `limit` bigint(11) NOT NULL DEFAULT '0' COMMENT '限制数量',
  `interval` bigint(11) NOT NULL DEFAULT '1' COMMENT '时间间隔(秒)',
  `service_id` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-禁用 1-启用',
  `type` varchar(10) DEFAULT 'url' COMMENT '限流规则类型:url,hosts',
  `rules` text COMMENT '限流规则内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网关流量限制';

-- ----------------------------
-- Records of gateway_rate_limit
-- ----------------------------
INSERT INTO `gateway_rate_limit` VALUES ('0', '1', 'admin-service', '1', '', null);
INSERT INTO `gateway_rate_limit` VALUES ('0', '1', 'admin-service', '1', 'url', '/api');

-- ----------------------------
-- Table structure for gateway_routes
-- ----------------------------
DROP TABLE IF EXISTS `gateway_routes`;
CREATE TABLE `gateway_routes` (
  `id` varchar(64) NOT NULL DEFAULT '' COMMENT '路由ID',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `service_id` varchar(255) DEFAULT NULL COMMENT '服务ID',
  `url` varchar(255) DEFAULT NULL COMMENT '完整地址',
  `strip_prefix` tinyint(1) DEFAULT '1' COMMENT '忽略前缀',
  `retryable` tinyint(1) DEFAULT '0' COMMENT '0-不重试 1-重试',
  `enabled` tinyint(1) DEFAULT '0' COMMENT '0-禁用 1-启用 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网关动态路由';

-- ----------------------------
-- Records of gateway_routes
-- ----------------------------
INSERT INTO `gateway_routes` VALUES ('club', '/club/**', '1', '', '1', '0', '1');
INSERT INTO `gateway_routes` VALUES ('user', '/user/**', '2', '', '1', '0', '1');
SET FOREIGN_KEY_CHECKS=1;
