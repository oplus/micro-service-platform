/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : open-platform

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2018-11-01 19:06:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for rbac_permission
-- ----------------------------
DROP TABLE IF EXISTS `rbac_permission`;
CREATE TABLE `rbac_permission` (
  `code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '授权编码: {权限拥有者}+{资源类型}+{资源名称}  user:api:getInfo',
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL COMMENT '资源ID',
  `resource_type` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '资源类型:api,menu,button',
  `owner_id` bigint(20) NOT NULL COMMENT '拥有者角色ID/拥有者用户ID',
  `owner_type` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '拥有者类型:user-个人 group-组'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='资源授权表';

-- ----------------------------
-- Records of rbac_permission
-- ----------------------------
INSERT INTO `rbac_permission` VALUES ('role:menu:home', null, '2', 'menu', '1', 'role');
INSERT INTO `rbac_permission` VALUES ('role:menu:home', null, '1', 'menu', '1', 'role');
INSERT INTO `rbac_permission` VALUES ('role:menu:home', null, '3', 'menu', '1', 'role');

-- ----------------------------
-- Table structure for rbac_resource_api
-- ----------------------------
DROP TABLE IF EXISTS `rbac_resource_api`;
CREATE TABLE `rbac_resource_api` (
  `api_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '资源ID',
  `api_code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '资源编码',
  `api_name` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '资源名称',
  `server_id` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '服务ID',
  `url` varchar(200) COLLATE utf8_bin NOT NULL COMMENT '资源路径',
  `priority` bigint(20) NOT NULL DEFAULT '0' COMMENT '优先级',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '资源描述',
  `enabled` bit(10) NOT NULL COMMENT '是否可用',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`api_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='API资源表';

-- ----------------------------
-- Records of rbac_resource_api
-- ----------------------------

-- ----------------------------
-- Table structure for rbac_resource_menu
-- ----------------------------
DROP TABLE IF EXISTS `rbac_resource_menu`;
CREATE TABLE `rbac_resource_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单Id',
  `menu_code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '菜单编码',
  `menu_name` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '菜单名称',
  `menu_title` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '菜单标题',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父级菜单',
  `url` varchar(200) COLLATE utf8_bin NOT NULL COMMENT '请求路径',
  `priority` bigint(20) NOT NULL DEFAULT '0' COMMENT '优先级 越小越靠前',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `enabled` int(10) NOT NULL DEFAULT '1' COMMENT '是否可用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='菜单表';

-- ----------------------------
-- Records of rbac_resource_menu
-- ----------------------------
INSERT INTO `rbac_resource_menu` VALUES ('1', 'system', '系统和安全', '系统和安全', null, '', '0', '系统和安全', '1', '2018-07-29 21:20:10', '2018-09-25 23:02:11');
INSERT INTO `rbac_resource_menu` VALUES ('2', 'authority', '权限管理', '权限管理', '1', '/authority/index', '0', '权限管理', '1', '2018-07-29 21:20:13', '2018-09-25 23:02:11');
INSERT INTO `rbac_resource_menu` VALUES ('3', 'menu', '菜单管理', '菜单管理', '1', '/menu/index', '0', '菜单管理', '1', '2018-07-29 21:20:13', '2018-09-25 23:02:11');
INSERT INTO `rbac_resource_menu` VALUES ('4', 'server', '服务和维护', '服务和维护', null, '', '0', '服务和维护', '1', '2018-07-29 21:20:13', '2018-09-25 23:02:11');
INSERT INTO `rbac_resource_menu` VALUES ('5', 'route', '网关路由', '网关路由', '4', '/gateway/route/index', '0', '网关路由', '1', '2018-07-29 21:20:13', '2018-09-25 23:02:11');
INSERT INTO `rbac_resource_menu` VALUES ('6', 'api', 'API管理', 'API管理', '4', '/gateway/api/index', '0', 'API管理', '1', '2018-07-29 21:20:13', '2018-09-25 23:02:11');

-- ----------------------------
-- Table structure for rbac_resource_operate
-- ----------------------------
DROP TABLE IF EXISTS `rbac_resource_operate`;
CREATE TABLE `rbac_resource_operate` (
  `operate_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '资源ID',
  `operate_code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '资源编码',
  `operate_name` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '资源名称',
  `url` varchar(200) COLLATE utf8_bin NOT NULL COMMENT '资源路径',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '资源父节点',
  `priority` int(10) NOT NULL DEFAULT '0' COMMENT '优先级 越小越靠前',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '资源描述',
  `enabled` int(10) NOT NULL COMMENT '是否可用',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`operate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='操作表';

-- ----------------------------
-- Records of rbac_resource_operate
-- ----------------------------
INSERT INTO `rbac_resource_operate` VALUES ('1', 'btnDetail', '详情', '/funcPages/adManager', '2', '0', '详情', '1', '2018-07-29 21:20:10', '2018-09-25 23:02:11');
INSERT INTO `rbac_resource_operate` VALUES ('2', 'btnSave', '保存', '/index', '2', '0', '保存', '1', '2018-07-29 21:20:13', '2018-09-25 23:02:11');

-- ----------------------------
-- Table structure for rbac_role
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role`;
CREATE TABLE `rbac_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '角色编码',
  `role_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '角色名称',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '角色描述',
  `enabled` int(10) NOT NULL COMMENT '是否可用',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_code` (`role_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色';

-- ----------------------------
-- Records of rbac_role
-- ----------------------------
INSERT INTO `rbac_role` VALUES ('1', 'superAdmin', '超级管理员', '', '1', '2018-07-29 21:14:50', '2018-07-29 21:14:53');
INSERT INTO `rbac_role` VALUES ('2', 'platfromAdmin', '平台管理员', '', '1', '2018-07-29 21:14:54', '2018-07-29 21:15:00');
INSERT INTO `rbac_role` VALUES ('3', 'developer', '普通开发者', '', '1', '2018-07-29 21:14:54', '2018-07-29 21:15:00');
INSERT INTO `rbac_role` VALUES ('4', 'companyDeveloper', '企业开发者', '', '1', '2018-07-29 21:14:54', '2018-07-29 21:15:00');

-- ----------------------------
-- Table structure for rbac_role_user
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_user`;
CREATE TABLE `rbac_role_user` (
  `user_id` bigint(32) NOT NULL COMMENT '用户ID',
  `role_id` bigint(32) NOT NULL COMMENT '角色ID',
  KEY `fk_user` (`user_id`),
  KEY `fk_role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色和用户关系表';

-- ----------------------------
-- Records of rbac_role_user
-- ----------------------------
INSERT INTO `rbac_role_user` VALUES ('1', '4');
INSERT INTO `rbac_role_user` VALUES ('1', '1');
INSERT INTO `rbac_role_user` VALUES ('1', '2');

-- ----------------------------
-- Table structure for rbac_user
-- ----------------------------
DROP TABLE IF EXISTS `rbac_user`;
CREATE TABLE `rbac_user` (
  `user_id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(100) DEFAULT NULL COMMENT '登录名',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '用户密码',
  `status` int(1) DEFAULT '1' COMMENT '状态:0-禁用 1-启用 2-锁定',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '最后一次登录IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='登录用户';

-- ----------------------------
-- Records of rbac_user
-- ----------------------------
INSERT INTO `rbac_user` VALUES ('1', 'admin', '$2a$10$qBdYE6SVFx566o.N7u.aseiRXg1RZElJX2d5NX9MgFhB2dMuM5Ncq', '1', '2018-09-13 00:40:14', '2018-09-13 00:40:17', null, null);
INSERT INTO `rbac_user` VALUES ('2', 'liuyadu', '$2a$10$qBdYE6SVFx566o.N7u.aseiRXg1RZElJX2d5NX9MgFhB2dMuM5Ncq', '1', '2018-09-13 00:40:34', '2018-09-13 00:40:36', null, null);
SET FOREIGN_KEY_CHECKS=1;
