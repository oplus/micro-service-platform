### 微服务快速开发平台  
     springCloud  Edgware.SR5 
     springCloud  eureka
     springCloud  zuul
     springCloud  config
#### 项目介绍
* docs(文档说明)  
    * bin          执行脚本
    * sql          sql文件
* common(公共模块)         
    * common-core 工具类、自动配置、全局异常解析与处理
    * generator-code 代码生成器
* cloud(分布式基础服务)     
    * discovery服务发现 (port = 8101)
    * config   配置中心 (port = 8151) 
    * gateway  网关服务 (port = 8888) ps:外部供外部统一调用服务,自身作为OAuth2客户端(client_credentials模式)
* upms(统一身份权限认证)
   * oauth-client  认证服务-客户端
   * oauth-service 认证管理-服务端 (port = 8211) ps:即作为认证服务器也作为资源服务器(提供用户信息)
   * rbac-client   权限管理-客户端
   * rbac-service  权限管理-服务端 (port = 8233)
     
#### 项目规划(开发中)
* app内容管理
    * 应用管理
        1. 应用配置
        2. 接口授权
    * 移动应用管理
        1. 版本升级
        2. 闪屏管理
        3. banner管理
        3. 广告管理
        4. 等...
* 短信模块(多渠道)
    * 短信渠道网关
    * 短信调用接口
    * 短信服务管理
        1.渠道商配置
        2.渠道路由配置
        3.短信模板
    * 短信服务-渠道A
    * 短信服务-渠道B
    
* 直播模块(多渠道)
    * 直播渠道网关
    * 直播调用接口
    * 直播服务管理
    * 直播服务-七牛
    * 直播服务-网易
    
####OAuth2介绍
 OAuth 2.0定义了四种授权方式。
   1. 密码模式（password）(为遗留系统设计)(支持refresh_token)
   2. 授权码模式（authorization_code）(正宗方式)(支持支持refresh_token)
   3. 简化模式（implicit）(为web浏览器应用设计)(不支持refresh_token)
   4. 客户端模式（client_credentials）(为后台api服务消费者设计)(不支持refresh_token)
#### 项目打包
    mvn clean install package -P {dev|test|pro}
#### 项目运行
    ./start.sh {start|stop|restart|status} register.jar
    启动顺序:
    1. discovery
    2. config
    3. rbac
    4. oauth
    5. gateway
#### 配置中心
    加密   
     curl http://localhost:8030/encrypt -d secret
    解密  
     curl localhost:8030/decrypt -d 682bc583f4641835fa2db009355293665d2647dade3375c0ee201de2a49f7bda secret
#### 消息总线
    刷新配置  
     curl -X POST http://192.168.1.12:1045/bus/refresh
    局部刷新  
     curl -X POST http://192.168.1.12:1045/bus/refresh?destination=admin-service:8000`
     curl -X POST http://192.168.1.12:1045/bus/refresh?destination=admin-service:**

