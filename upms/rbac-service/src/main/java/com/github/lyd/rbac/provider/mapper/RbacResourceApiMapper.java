package com.github.lyd.rbac.provider.mapper;

import com.github.lyd.common.mapper.BaseMapper;
import com.github.lyd.rbac.client.entity.RbacResourceApi;

public interface RbacResourceApiMapper extends BaseMapper<RbacResourceApi> {
}