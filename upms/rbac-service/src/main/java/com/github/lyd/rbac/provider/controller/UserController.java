package com.github.lyd.rbac.provider.controller;

import com.github.lyd.common.model.dto.ResponseData;
import com.github.lyd.rbac.client.dto.UserLoginInfo;
import com.github.lyd.rbac.client.service.UserClient;
import com.github.lyd.rbac.provider.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController implements UserClient {
    @Autowired
    private UserService userService;

    /**
     * 获取用户信息
     *
     * @param username 登录名
     * @return
     */
    @Override
    public ResponseData<UserLoginInfo> login(String username) {
        return ResponseData.ok(userService.getUserLoginInfo(username));
    }
}
