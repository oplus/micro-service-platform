package com.github.lyd.rbac.provider.mapper;

import com.github.lyd.common.mapper.BaseMapper;
import com.github.lyd.rbac.client.entity.RbacResourceMenu;

public interface RbacResourceMenuMapper extends BaseMapper<RbacResourceMenu> {
}