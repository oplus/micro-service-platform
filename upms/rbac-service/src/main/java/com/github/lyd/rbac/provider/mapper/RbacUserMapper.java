package com.github.lyd.rbac.provider.mapper;

import com.github.lyd.common.mapper.BaseMapper;
import com.github.lyd.rbac.client.entity.RbacUser;

public interface RbacUserMapper extends BaseMapper<RbacUser> {
}