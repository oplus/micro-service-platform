package com.github.lyd.rbac.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;


/**
 * @author liuyadu
 */

@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(basePackages = "com.github.lyd.rbac.provider.mapper")
@ComponentScan(basePackages = {"com.github.lyd"})
public class RbacApplication {

    public static void main(String[] args) {
        SpringApplication.run(RbacApplication.class, args);
    }
}
