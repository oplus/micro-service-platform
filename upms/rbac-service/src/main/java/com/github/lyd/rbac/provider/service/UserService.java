package com.github.lyd.rbac.provider.service;

import com.github.lyd.rbac.client.dto.UserLoginInfo;

/**
 * @author: liuyadu
 * @date: 2018/10/24 16:38
 * @description:
 */
public interface UserService {

    /**
     * 获取用户登陆信息
     *
     * @param username 登录名
     * @return
     */
    UserLoginInfo getUserLoginInfo(String username);
}
