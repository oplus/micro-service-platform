package com.github.lyd.rbac.provider.service.impl;

import com.github.lyd.common.utils.QueryBuilder;
import com.github.lyd.common.utils.StringUtils;
import com.github.lyd.rbac.client.constans.RbacConstans;
import com.github.lyd.rbac.client.dto.UserLoginInfo;
import com.github.lyd.rbac.client.entity.RbacPermission;
import com.github.lyd.rbac.client.entity.RbacRole;
import com.github.lyd.rbac.client.entity.RbacUser;
import com.github.lyd.rbac.provider.mapper.RbacPermissionMapper;
import com.github.lyd.rbac.provider.mapper.RbacRoleMapper;
import com.github.lyd.rbac.provider.mapper.RbacUserMapper;
import com.github.lyd.rbac.provider.service.UserService;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author: liuyadu
 * @date: 2018/10/24 16:33
 * @description:
 */
@Service
public class UserSeviceImpl implements UserService {

    @Autowired
    private RbacUserMapper rbacUserMapper;
    @Autowired
    private RbacRoleMapper rbacRoleMapper;
    @Autowired
    private RbacPermissionMapper rbacPermissionMapper;

    /**
     * 获取用户登陆信息
     *
     * @param username 登录名
     * @return
     */
    @Override
    public UserLoginInfo getUserLoginInfo(String username) {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        QueryBuilder builder = new QueryBuilder(RbacUser.class);
        Example example = builder.and("username", QueryBuilder.OP.EQ, username).build();
        RbacUser rbacUser = rbacUserMapper.selectOneByExample(example);
        if (rbacUser == null) {
            return null;
        }
        UserLoginInfo userLoginInfo = new UserLoginInfo();
        BeanUtils.copyProperties(rbacUser, userLoginInfo);

        List<RbacRole> roles = rbacRoleMapper.findUserRoles(rbacUser.getUserId());
        List<Long> roleIds = Lists.newArrayList();
        roles.forEach(rbacRole -> {
            roleIds.add(rbacRole.getRoleId());
        });
        List<RbacPermission> permissions = Lists.newArrayList();

        // 角色权限
        if (!roleIds.isEmpty()) {
            builder = new QueryBuilder(RbacPermission.class);
            example = builder.and("ownerType", QueryBuilder.OP.EQ, RbacConstans.PERMISSION_OWNER_ROLE)
                    .and("ownerId", QueryBuilder.OP.IN, roleIds).build();
            List<RbacPermission> rolePermissions = rbacPermissionMapper.selectByExample(example);
            if (rolePermissions != null) {
                permissions.addAll(rolePermissions);
            }
        }

        // 用户私有权限
        builder = new QueryBuilder(RbacPermission.class);
        example = builder.and("ownerType", QueryBuilder.OP.EQ, RbacConstans.PERMISSION_OWNER_USER)
                .and("ownerId", QueryBuilder.OP.EQ, rbacUser.getUserId()).build();
        List<RbacPermission> userPermissions = rbacPermissionMapper.selectByExample(example);
        if (userPermissions != null) {
            permissions.addAll(userPermissions);
        }
        userLoginInfo.setPermissions(permissions);
        userLoginInfo.setRoles(roles);
        return userLoginInfo;
    }
}
