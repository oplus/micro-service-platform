package com.github.lyd.rbac.provider.mapper;

import com.github.lyd.common.mapper.BaseMapper;
import com.github.lyd.rbac.client.entity.RbacRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RbacRoleMapper extends BaseMapper<RbacRole> {

    List<RbacRole> findUserRoles(@Param("userId") Long userId);
}