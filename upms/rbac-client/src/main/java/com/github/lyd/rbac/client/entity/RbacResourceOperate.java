package com.github.lyd.rbac.client.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 操作表
 *
 * @author: liuyadu
 * @date: 2018/10/24 16:21
 * @description:
 */
@Table(name = "rbac_resource_operate")
public class RbacResourceOperate implements Serializable {
    private static final long serialVersionUID = 1471599074044557390L;
    /**
     * 资源ID
     */
    @Id
    @Column(name = "operate_id")
    private Long operateId;

    /**
     * 资源编码
     */
    @Column(name = "operate_code")
    private String operateCode;

    /**
     * 资源名称
     */
    @Column(name = "operate_name")
    private String operateName;

    /**
     * 资源路径
     */
    private String url;

    /**
     * 资源父节点
     */
    @Column(name = "menu_id")
    private Long menuId;

    /**
     * 优先级 越小越靠前
     */
    private Integer priority;

    /**
     * 资源描述
     */
    private String description;

    /**
     * 是否可用
     */
    private Integer enabled;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取资源ID
     *
     * @return operate_id - 资源ID
     */
    public Long getOperateId() {
        return operateId;
    }

    /**
     * 设置资源ID
     *
     * @param operateId 资源ID
     */
    public void setOperateId(Long operateId) {
        this.operateId = operateId;
    }

    /**
     * 获取资源编码
     *
     * @return operate_code - 资源编码
     */
    public String getOperateCode() {
        return operateCode;
    }

    /**
     * 设置资源编码
     *
     * @param operateCode 资源编码
     */
    public void setOperateCode(String operateCode) {
        this.operateCode = operateCode;
    }

    /**
     * 获取资源名称
     *
     * @return operate_name - 资源名称
     */
    public String getOperateName() {
        return operateName;
    }

    /**
     * 设置资源名称
     *
     * @param operateName 资源名称
     */
    public void setOperateName(String operateName) {
        this.operateName = operateName;
    }

    /**
     * 获取资源路径
     *
     * @return url - 资源路径
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置资源路径
     *
     * @param url 资源路径
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取资源父节点
     *
     * @return menu_id - 资源父节点
     */
    public Long getMenuId() {
        return menuId;
    }

    /**
     * 设置资源父节点
     *
     * @param menuId 资源父节点
     */
    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    /**
     * 获取优先级 越小越靠前
     *
     * @return priority - 优先级 越小越靠前
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * 设置优先级 越小越靠前
     *
     * @param priority 优先级 越小越靠前
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * 获取资源描述
     *
     * @return description - 资源描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置资源描述
     *
     * @param description 资源描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取是否可用
     *
     * @return enabled - 是否可用
     */
    public Integer getEnabled() {
        return enabled;
    }

    /**
     * 设置是否可用
     *
     * @param enabled 是否可用
     */
    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}