package com.github.lyd.rbac.client.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 资源授权表
 *
 * @author: liuyadu
 * @date: 2018/10/24 16:21
 * @description:
 */
@Table(name = "rbac_permission")
public class RbacPermission implements Serializable {
    private static final long serialVersionUID = 3218590056425312760L;
    /**
     * 授权编码: {权限拥有者}+{资源类型}+{资源名称}  user:api:getInfo
     */
    private String code;

    private String url;

    /**
     * 资源ID
     */
    @Column(name = "resource_id")
    private Integer resourceId;

    /**
     * 资源类型:api,menu,button
     */
    @Column(name = "resource_type")
    private String resourceType;

    /**
     * 拥有者角色ID/拥有者用户ID
     */
    @Column(name = "owner_id")
    private Long ownerId;

    /**
     * 拥有者类型:user-个人 group-组
     */
    @Column(name = "owner_type")
    private String ownerType;

    /**
     * 获取授权编码: {权限拥有者}+{资源类型}+{资源名称}  user:api:getInfo
     *
     * @return code - 授权编码: {权限拥有者}+{资源类型}+{资源名称}  user:api:getInfo
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置授权编码: {权限拥有者}+{资源类型}+{资源名称}  user:api:getInfo
     *
     * @param code 授权编码: {权限拥有者}+{资源类型}+{资源名称}  user:api:getInfo
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取资源ID
     *
     * @return resource_id - 资源ID
     */
    public Integer getResourceId() {
        return resourceId;
    }

    /**
     * 设置资源ID
     *
     * @param resourceId 资源ID
     */
    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * 获取资源类型:api,menu,button
     *
     * @return resource_type - 资源类型:api,menu,button
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * 设置资源类型:api,menu,button
     *
     * @param resourceType 资源类型:api,menu,button
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * 获取拥有者角色ID/拥有者用户ID
     *
     * @return owner_id - 拥有者角色ID/拥有者用户ID
     */
    public Long getOwnerId() {
        return ownerId;
    }

    /**
     * 设置拥有者角色ID/拥有者用户ID
     *
     * @param ownerId 拥有者角色ID/拥有者用户ID
     */
    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * 获取拥有者类型:user-个人 group-组
     *
     * @return owner_type - 拥有者类型:user-个人 group-组
     */
    public String getOwnerType() {
        return ownerType;
    }

    /**
     * 设置拥有者类型:user-个人 group-组
     *
     * @param ownerType 拥有者类型:user-个人 group-组
     */
    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }
}