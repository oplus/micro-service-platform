package com.github.lyd.rbac.client.constans;

/**
 * 通用权限常量
 */
public class RbacConstans {
    /**
     * 用户状态
     * 0:禁用,1:正常,2:锁定
     */
    public final static int USER_STATUS_DISABLE = 0;
    public final static int USER_STATUS_NORMAL = 1;
    public final static int USER_STATUS_LOCKED = 2;

    /**
     * 资源类型
     * MENU:菜单,ACTION:操作,API:API路由,FILE:文件
     */
    public String RESOURCE_TYPE_MENU = "menu";
    public String RESOURCE_TYPE_OPERATE = "operate";
    public String RESOURCE_TYPE_API = "api";

    /**
     * 权限所有者
     * user:用户权限 role:角色权限
     */
    public final static String PERMISSION_SEPARATOR = ":";
    public final static String PERMISSION_OWNER_USER = "user";
    public final static String PERMISSION_OWNER_ROLE = "role";
}
