package com.github.lyd.rbac.client.dto;

import com.github.lyd.rbac.client.entity.RbacPermission;
import com.github.lyd.rbac.client.entity.RbacRole;
import com.github.lyd.rbac.client.entity.RbacUser;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * @author: liuyadu
 * @date: 2018/10/24 16:21
 * @description:
 */
public class UserLoginInfo extends RbacUser implements Serializable {
    private static final long serialVersionUID = 5281748359923501640L;

    private List<RbacRole> roles = Lists.newArrayList();

    private List<RbacPermission> permissions = Lists.newArrayList();

    public List<RbacRole> getRoles() {
        return roles;
    }

    public void setRoles(List<RbacRole> roles) {
        this.roles = roles;
    }

    public List<RbacPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<RbacPermission> permissions) {
        this.permissions = permissions;
    }
}
