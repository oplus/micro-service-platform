package com.github.lyd.sso.auth.provider.controller;

import com.github.lyd.common.model.dto.ResponseData;
import com.github.lyd.common.utils.RandomValueUtils;
import com.github.lyd.sso.auth.client.dto.ClientInfo;
import com.github.lyd.sso.auth.client.service.ClientInfoClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.ClientAlreadyExistsException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liuyadu
 * @date: 2018/11/2 11:47
 * @description:
 */
@RestController
@Api(tags = "认证客户端信息")
public class ClientInfoController implements ClientInfoClient {

    @Autowired
    private JdbcClientDetailsService jdbcClientDetailsService;

    /**
     * 获取所有客户端
     *
     * @return
     */
    @Override
    @ApiOperation(value = "获取所有客户端")
    public ResponseData<ClientInfo> clients() {
        return ResponseData.ok(jdbcClientDetailsService.listClientDetails());
    }

    /**
     * 添加客户端
     * @param appName      应用名称
     * @param appType      应用类型:server-应用服务 mobile-手机应用 browser-浏览器
     * @param appDesc      应用说明
     * @param os           手机应用操作系统:ios-苹果 android-安卓
     * @param isAutoApprove 是否自动授权
     * @param redirectUrls  重定向地址
     * @param grantTypes    授权方式
     * @param scopes        授权范围
     * @param resourceIds   资源服务器ID
     * @param authorities   客户端权限,多个用逗号隔开
     * @return 客户端信息
     */
    @ApiOperation(value = "添加客户端")
    @Override
    public ResponseData<ClientInfo> addClient(
            @RequestParam String appName,
            @RequestParam String appType,
            @RequestParam(required = false) String appDesc,
            @RequestParam String os,
            @RequestParam boolean isAutoApprove,
            @RequestParam String redirectUrls,
            @RequestParam String grantTypes,
            @RequestParam String scopes,
            @RequestParam(required = false) String resourceIds,
            @RequestParam(required = false) String authorities
    ) {
        String clientId = RandomValueUtils.uuid();
        String clientSecret = RandomValueUtils.uuid();
        ClientInfo clientInfo = new ClientInfo(appName, appType, appDesc, os, clientId, clientSecret, resourceIds, scopes, grantTypes, redirectUrls, authorities);
        try {
            jdbcClientDetailsService.addClientDetails(clientInfo);
        } catch (ClientAlreadyExistsException e) {
            return ResponseData.failed("客户端已存在！");
        }
        return ResponseData.ok(clientInfo);
    }

    /**
     * 更新客户端
     * @param appName      应用名称
     * @param appType      应用类型:server-应用服务 mobile-手机应用 browser-浏览器
     * @param appDesc      应用说明
     * @param os           手机应用操作系统:ios-苹果 android-安卓
     * @param clientId      客户端Id
     * @param isAutoApprove 是否自动授权
     * @param redirectUrls  重定向地址
     * @param grantTypes    授权方式
     * @param scopes        授权范围
     * @param resourceIds   资源服务器ID
     * @param authorities   客户端权限,多个用逗号隔开
     * @return 客户端信息
     */
    @ApiOperation(value = "更新客户端")
    @Override
    public ResponseData updateClient(
            @RequestParam String appName,
            @RequestParam String appType,
            @RequestParam(required = false) String appDesc,
            @RequestParam String os,
            @PathVariable("clientId") String clientId,
            @RequestParam boolean isAutoApprove,
            @RequestParam String redirectUrls,
            @RequestParam String grantTypes,
            @RequestParam String scopes,
            @RequestParam(required = false) String resourceIds,
            @RequestParam(required = false) String authorities) {
        ClientInfo clientInfo = new ClientInfo(appName,appType,appDesc,os,clientId, null, resourceIds, scopes, grantTypes, redirectUrls, authorities);
        try {
            jdbcClientDetailsService.updateClientDetails(clientInfo);
        } catch (NoSuchClientException e) {
            return ResponseData.failed("客户端不存在！");
        }
        return ResponseData.ok();
    }

    /**
     * 重置秘钥
     *
     * @param clientId 客户端Id
     * @return 客户端信息
     */
    @ApiOperation(value = "重置秘钥")
    @Override
    public ResponseData<String> resetSecret(@PathVariable("clientId") String clientId) {
        String clientSecret = RandomValueUtils.uuid();
        try {
            jdbcClientDetailsService.updateClientSecret(clientId, clientSecret);
        } catch (NoSuchClientException e) {
            return ResponseData.failed("客户端不存在！");
        }
        return ResponseData.ok(clientSecret);
    }

    /**
     * 获取应用信息
     *
     * @param clientId 客户端Id
     * @return 客户端信息
     */
    @ApiOperation(value = "获取应用信息")
    @Override
    public ResponseData<ClientInfo> getClient(@PathVariable("clientId") String clientId) {
        ClientDetails clientInfo = null;
        try {
            clientInfo = jdbcClientDetailsService.loadClientByClientId(clientId);
        } catch (InvalidClientException e) {
            return ResponseData.failed("客户端不存在！");
        }
        return ResponseData.ok(clientInfo);
    }

    /**
     * 删除应用
     *
     * @param clientId 客户端Id
     * @return 客户端信息
     */
    @ApiOperation(value = "删除应用")
    @Override
    public ResponseData<ClientInfo> delete(@PathVariable("clientId") String clientId) {
        try {
            jdbcClientDetailsService.removeClientDetails(clientId);
        } catch (NoSuchClientException e) {
            return ResponseData.failed("客户端不存在！");
        }
        return ResponseData.ok("删除成功");
    }


}
