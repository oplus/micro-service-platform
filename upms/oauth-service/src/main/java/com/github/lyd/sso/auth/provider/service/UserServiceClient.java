package com.github.lyd.sso.auth.provider.service;

import com.github.lyd.common.config.FeignRequestConfig;
import com.github.lyd.rbac.client.service.UserClient;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author: liuyadu
 * @date: 2018/10/24 16:49
 * @description:
 */
@FeignClient(value = "rbac",configuration = FeignRequestConfig.class)
public interface UserServiceClient extends UserClient {


}
