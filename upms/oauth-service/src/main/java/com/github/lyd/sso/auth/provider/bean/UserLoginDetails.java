package com.github.lyd.sso.auth.provider.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.lyd.rbac.client.constans.RbacConstans;
import com.github.lyd.rbac.client.dto.UserLoginInfo;
import com.google.common.collect.Lists;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author liuyadu
 */
public class UserLoginDetails implements UserDetails {
    private static final long serialVersionUID = 1L;
    protected UserLoginInfo userLoginInfo;

    public UserLoginDetails(UserLoginInfo userLoginInfo) {
        this.userLoginInfo = userLoginInfo;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = Lists.newArrayList();
        if (userLoginInfo.getRoles() != null) {
            userLoginInfo.getRoles().forEach(role -> {
                authorities.add(new SimpleGrantedAuthority(role.getRoleCode().startsWith("ROLE_") ? role.getRoleCode() : "ROLE_" + role.getRoleCode()));
            });
        }
        return authorities;
    }

    @JsonIgnore
    @JSONField(serialize = false)
    @Override
    public String getPassword() {
        return userLoginInfo.getPassword();
    }

    @Override
    public String getUsername() {
        return userLoginInfo.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return userLoginInfo.getStatus().intValue() != RbacConstans.USER_STATUS_LOCKED;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return userLoginInfo.getStatus().intValue() == RbacConstans.USER_STATUS_NORMAL ? true : false;
    }

    public String getUserId() {
        return String.valueOf(userLoginInfo.getUserId());
    }

    public Date getCreateTime() {
        return userLoginInfo.getCreateTime();
    }

    public Date getUpdateTime() {
        return userLoginInfo.getUpdateTime();
    }

    public String getLastLoginIp() {
        return userLoginInfo.getLastLoginIp();
    }

    public Date getLastLoginTime() {
        return userLoginInfo.getLastLoginTime();
    }

}