package com.github.lyd.sso.auth.provider.service.impl;

import com.github.lyd.common.model.dto.ResponseData;
import com.github.lyd.rbac.client.dto.UserLoginInfo;
import com.github.lyd.sso.auth.provider.bean.UserLoginDetails;
import com.github.lyd.sso.auth.provider.service.UserServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 为了安全这里直接用jdbc 访问用户表, 由于安全限制不能直接使用feign方式获取用户信息
 *
 * @author liuyadu
 */
@Service("userDetailService")
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserServiceClient userServiceClient;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        ResponseData<UserLoginInfo> resp = userServiceClient.login(username);
        UserLoginInfo userLoginInfo = resp.getData();
        if (userLoginInfo == null) {
            throw new UsernameNotFoundException("用户 " + username + " 不存在!");
        }
        return new UserLoginDetails(userLoginInfo);
    }
}