package com.github.lyd.sso.auth.provider.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

/**
 * @author: liuyadu
 * @date: 2018/10/29 15:59
 * @description:
 */
@Controller
public class IndexController {

    /**
     * 欢迎页
     * @return
     */
    @GetMapping("/")
    public String welcome() {
        return "welcome";
    }

    /**
     * 登录页
     *
     * @return
     */
    @GetMapping("/login")
    public String login() {
        return "login";
    }

    /**
     * 获取当前登录用户
     *
     * @param principal
     * @return
     */
    @GetMapping("/user")
    Principal user(Principal principal) {
        return principal;
    }
}
