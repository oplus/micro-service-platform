package com.github.lyd.sso.auth.client.service;

import com.github.lyd.common.model.dto.ResponseData;
import com.github.lyd.sso.auth.client.dto.ClientInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author: liuyadu
 * @date: 2018/11/2 12:05
 * @description:
 */
public interface ClientInfoClient {
    /**
     * 获取所有客户端
     *
     * @return
     */
    @GetMapping("/clients")
    ResponseData<ClientInfo> clients();

    /**
     * 添加客户端
     * @param appName      应用名称
     * @param appType      应用类型:server-应用服务 mobile-手机应用 browser-浏览器
     * @param appDesc      应用说明
     * @param os           手机应用操作系统:ios-苹果 android-安卓
     * @param isAutoApprove 是否自动授权
     * @param redirectUrls  重定向地址
     * @param grantTypes    授权方式
     * @param scopes        授权范围
     * @param resourceIds   资源服务器ID
     * @param authorities   客户端权限,多个用逗号隔开
     * @return 客户端信息
     */
    @PostMapping("/clients/add")
    ResponseData<ClientInfo> addClient(
            @RequestParam String appName,
            @RequestParam String appType,
            @RequestParam(required = false) String appDesc,
            @RequestParam String os,
            @RequestParam boolean isAutoApprove,
            @RequestParam String redirectUrls,
            @RequestParam String grantTypes,
            @RequestParam String scopes,
            @RequestParam(required = false) String resourceIds,
            @RequestParam(required = false) String authorities
    );

    /**
     * 更新客户端
     * @param appName      应用名称
     * @param appType      应用类型:server-应用服务 mobile-手机应用 browser-浏览器
     * @param appDesc      应用说明
     * @param os           手机应用操作系统:ios-苹果 android-安卓
     * @param clientId      客户端Id
     * @param isAutoApprove 是否自动授权
     * @param redirectUrls  重定向地址
     * @param grantTypes    授权方式
     * @param scopes        授权范围
     * @param resourceIds   资源服务器ID
     * @param authorities   客户端权限,多个用逗号隔开
     * @return 客户端信息
     */
    @PostMapping("/clients/{clientId}/update")
    ResponseData updateClient(
            @RequestParam String appName,
            @RequestParam String appType,
            @RequestParam(required = false) String appDesc,
            @RequestParam String os,
            @PathVariable("clientId") String clientId,
            @RequestParam boolean isAutoApprove,
            @RequestParam String redirectUrls,
            @RequestParam String grantTypes,
            @RequestParam String scopes,
            @RequestParam(required = false) String resourceIds,
            @RequestParam(required = false) String authorities
    );

    /**
     * 重置秘钥
     *
     * @param clientId 客户端Id
     * @return 客户端信息
     */
    @PostMapping("/clients/{clientId}/reset")
    ResponseData<String> resetSecret(
            @PathVariable("clientId") String clientId
    );


    /**
     * 获取应用信息
     *
     * @param clientId 客户端Id
     * @return 客户端信息
     */
    @GetMapping("/clients/{clientId}")
    ResponseData<ClientInfo> getClient(
            @PathVariable("clientId") String clientId
    );

    /**
     * 删除应用
     *
     * @param clientId 客户端Id
     * @return 客户端信息
     */
    @PostMapping("/clients/{clientId}/delete")
    ResponseData delete(
            @PathVariable("clientId") String clientId
    );
}
