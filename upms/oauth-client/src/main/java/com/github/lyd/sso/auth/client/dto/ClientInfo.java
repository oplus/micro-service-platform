package com.github.lyd.sso.auth.client.dto;

import com.github.lyd.sso.auth.client.constans.AuthConstans;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.*;

/**
 * @author: liuyadu
 * @date: 2018/11/2 18:02
 * @description:
 */
public class ClientInfo implements Serializable, ClientDetails {
    private static final long serialVersionUID = 3725084953460581042L;

    private String clientId;
    private String clientSecret;
    private String resourceIds;
    private String scopes;
    private String grantTypes;
    private String redirectUrls;
    private String authorities;
    private Map<String, Object> clientInfo;

    /**
     * @param appName      应用名称
     * @param appType      应用类型:server-应用服务 mobile-手机应用 browser-浏览器
     * @param appDesc      应用说明
     * @param os           手机应用操作系统:ios-苹果 android-安卓
     * @param clientId     客户端ID
     * @param clientSecret 客户端秘钥
     * @param resourceIds  资源服务ID
     * @param scopes       授权范围
     * @param grantTypes   授权类型
     * @param redirectUrls 授权重定向地址
     * @param authorities  权限
     */
    public ClientInfo(String appName, String appType, String appDesc, String os, String clientId, String clientSecret, String resourceIds, String scopes, String grantTypes, String redirectUrls, String authorities) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.resourceIds = resourceIds;
        this.scopes = scopes;
        this.grantTypes = grantTypes;
        this.redirectUrls = redirectUrls;
        this.authorities = authorities;
        this.clientInfo = Maps.newHashMap();
        this.clientInfo.put("appName",appName);
        this.clientInfo.put("appType",appType);
        this.clientInfo.put("appDesc",appDesc);
        this.clientInfo.put("os",os);
    }

    @Override
    public String getClientId() {
        return this.clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        return this.resourceIds != null ? new HashSet<>(Arrays.asList(StringUtils
                .tokenizeToStringArray(this.resourceIds, ","))) : null;
    }

    @Override
    public boolean isSecretRequired() {
        return true;
    }

    @Override
    public String getClientSecret() {
        return this.clientSecret;
    }

    @Override
    public boolean isScoped() {
        return false;
    }

    @Override
    public Set<String> getScope() {
        return this.scopes != null ? new HashSet<>(Arrays.asList(StringUtils
                .tokenizeToStringArray(this.scopes, ","))) : null;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return this.grantTypes != null ? new HashSet<>(Arrays.asList(StringUtils
                .tokenizeToStringArray(this.grantTypes, ","))) : null;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return this.redirectUrls != null ? new HashSet<>(Arrays.asList(StringUtils
                .tokenizeToStringArray(this.redirectUrls, ","))) : null;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        if (this.authorities != null) {
            List<GrantedAuthority> grantedAuthorityList = null;
            List<String> list = Arrays.asList(StringUtils.tokenizeToStringArray(this.authorities, ","));
            if (list.size() > 0) {
                grantedAuthorityList = Lists.newArrayList();
                List<GrantedAuthority> finalGrantedAuthorityList = grantedAuthorityList;
                list.forEach(a -> {
                    finalGrantedAuthorityList.add(new SimpleGrantedAuthority(a));
                });
            }
            return grantedAuthorityList;
        }
        return null;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return AuthConstans.ACCESS_TOKEN_VALIDITY_SECONDS;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return AuthConstans.REFRESH_TOKEN_VALIDITY_SECONDS;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return true;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return clientInfo;
    }
}
