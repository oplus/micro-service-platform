package com.github.lyd.gateway.config;

import com.github.lyd.gateway.route.RouteLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 动态路由配置
 *
 * @author: liuyadu
 * @date: 2018/10/23 10:31
 * @description:
 */
@Configuration
public class RouteConfig {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ZuulProperties zuulProperties;
    @Autowired
    ServerProperties server;

    /**
     * 初始化路由加载器
     *
     * @return
     */
    @Bean
    public RouteLocator zuulRouteLocator() {
        RouteLocator locator = new RouteLocator(server.getServletPrefix(), zuulProperties);
        locator.setJdbcTemplate(jdbcTemplate);
        return locator;
    }
}
