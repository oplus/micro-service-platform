package com.github.lyd.gateway.controller;

import com.github.lyd.common.model.dto.ResponseData;
import com.github.lyd.gateway.service.RefreshRateLimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 路由舒心
 * @author: liuyadu
 * @date: 2018/10/23 10:31
 * @description:
 */
@RestController
public class RateLimitRefreshController {
    @Autowired
    RefreshRateLimitService rateLimitService;

    @GetMapping("/rateLimit/refresh")
    public ResponseData refresh() {
        rateLimitService.refreshRateLimit();
        return ResponseData.ok("refresh success");
    }

}