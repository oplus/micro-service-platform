package com.github.lyd.gateway.ratelimit;

import org.springframework.context.ApplicationEvent;

public class RateLimitRefreshedEvent extends ApplicationEvent {
    private RateLimitLocator locator;

    public RateLimitRefreshedEvent(RateLimitLocator locator) {
        super(locator);
        this.locator = locator;
    }

    public RateLimitLocator getLocator() {
        return this.locator;
    }
}