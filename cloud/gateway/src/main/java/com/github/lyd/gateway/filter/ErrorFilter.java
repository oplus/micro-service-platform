package com.github.lyd.gateway.filter;

import com.github.lyd.common.exception.handler.GlobalExceptionHandler;
import com.github.lyd.common.model.dto.ResponseData;
import com.github.lyd.common.utils.WebUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ErrorFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "error";
    }

    @Override
    public int filterOrder() {
        return 10;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        Throwable ex = ctx.getThrowable();
        ResponseData responseData = GlobalExceptionHandler.resolveException((Exception) ex, ctx.getRequest(), ctx.getResponse());
        WebUtils.renderString(ctx.getResponse(), responseData);
        return null;
    }
}
