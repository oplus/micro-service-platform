package com.github.lyd.gateway.service;

import com.github.lyd.gateway.ratelimit.RateLimitLocator;
import com.github.lyd.gateway.ratelimit.RateLimitRefreshedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * 动态限流刷新服务
 */
@Service
public class RefreshRateLimitService {
    @Autowired
    ApplicationEventPublisher publisher;

    @Autowired
    RateLimitLocator rateLimitLocator;

    public void refreshRateLimit() {
        RateLimitRefreshedEvent routesRefreshedEvent = new RateLimitRefreshedEvent(rateLimitLocator);
        publisher.publishEvent(routesRefreshedEvent);
    }
}