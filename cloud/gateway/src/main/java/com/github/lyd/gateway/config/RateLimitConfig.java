package com.github.lyd.gateway.config;

import com.github.lyd.gateway.ratelimit.RateLimitLocator;
import com.github.lyd.gateway.ratelimit.RateLimitRefreshedEvent;
import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.properties.RateLimitProperties;
import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.support.StringToMatchTypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 动态限流配置
 *
 * @author: liuyadu
 * @date: 2018/10/23 10:31
 * @description:
 */
@Configuration
public class RateLimitConfig {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    RateLimitProperties rateLimitProperties;
    @Autowired
    StringToMatchTypeConverter converter;

    /**
     * 初始化限流加载器
     *
     * @return
     */
    @Bean
    public RateLimitLocator rateLimitLocator() {
        RateLimitLocator locator = new RateLimitLocator();
        locator.setJdbcTemplate(jdbcTemplate);
        locator.setConverter(converter);
        locator.setProperties(rateLimitProperties);
        locator.locatePolicy();
        return locator;
    }

    /**
     * 动态限流刷新监听
     *
     * @return
     */
    @Bean
    public RateLimitRefreshListener rateLimitRefreshListener() {
        return new RateLimitRefreshListener();
    }

    private static class RateLimitRefreshListener implements ApplicationListener<ApplicationEvent> {
        @Override
        public void onApplicationEvent(ApplicationEvent event) {
            if (event instanceof RateLimitRefreshedEvent) {
                ((RateLimitRefreshedEvent) event).getLocator().refresh();
            }
        }
    }
}
