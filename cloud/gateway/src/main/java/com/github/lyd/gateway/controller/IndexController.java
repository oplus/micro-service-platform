package com.github.lyd.gateway.controller;

import com.github.lyd.common.utils.WebUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: liuyadu
 * @date: 2018/11/5 16:33
 * @description:
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String index(HttpServletRequest request) {
        System.out.println(WebUtils.getParameterMap(request));
        return "index";
    }
}
