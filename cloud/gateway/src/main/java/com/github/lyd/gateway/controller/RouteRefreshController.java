package com.github.lyd.gateway.controller;

import com.github.lyd.common.model.dto.ResponseData;
import com.github.lyd.gateway.service.RefreshRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 路由舒心
 *
 * @author: liuyadu
 * @date: 2018/10/23 10:31
 * @description:
 */
@RestController
public class RouteRefreshController {
    @Autowired
    RefreshRouteService zuulRefreshRouteService;

    @GetMapping("/route/refresh")
    public ResponseData refresh() {
        zuulRefreshRouteService.refreshRoute();
        return ResponseData.ok("refresh success");
    }
}