package com.github.lyd.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Slf4j
public class SignUtils {


    public static void main(String[] args) {
        //参数签名算法测试例子
        HashMap<String, String> signMap = new HashMap<String, String>();
        signMap.put("clientId", "BC5549D899ED");
        signMap.put("userId", "1");
        signMap.put("type", "worker");
        signMap.put("signType", "SHA256");
        signMap.put("name", "中文测试");
        System.out.println("得到签名sign1:" + getSign(signMap, "e10adc3949ba59abbe56e057f20f883f"));
    }

//    public boolean validateSign(Map<String, String> paramMap) {
//        // TODO Auto-generated method stub
//        String sign = Pub_Tools.getString_TrimZeroLenAsNull(paramMap.get("sign"));
//        if(sign==null){
//            return false;
//        }
//        //获取AccessKey 下面这部分，可以针对用于做认证，此处暂时忽略
////		String AccessKey = Pub_Tools.getString_TrimZeroLenAsNull(paramMap.get("AccessKey"));
////		String SecretKey = null;
////		if(AccessKey==null){
////			return false;
////		}else{
////			//判断当前AccessKey是否是正确的。通过查询用户信息判断,并查询得到当前用户的SecretKey
////			VTS_User vts_User = com_VTSUserDao.getByKey(AccessKey);
////			SecretKey = vts_User.getSecretKey();
////			if(SecretKey==null){
////				return false;
////			}
////		}
//        //第一步判断时间戳 timestamp=201808091113
//        //和服务器时间差值在10分钟以上不予处理
//        String timestamp = Pub_Tools.getString_TrimZeroLenAsNull(paramMap.get("timestamp"));
//        //如果没有带时间戳返回
//        if(timestamp==null){
//            return false;
//        }else{
//            try {
//                Date clientDate = new SimpleDateFormat("yyyyMMddHHmmss").parse(timestamp);
//                if((System.currentTimeMillis()-clientDate.getTime())>(10*60*1000L)){//时间差值过大
//                    return false;
//                }
//                //第二步获取随机值
//                String nonce = Pub_Tools.getString_TrimZeroLenAsNull(paramMap.get("nonce"));
//                if(nonce==null){//非法请求
//                    return false;
//                }else{//判断请求是否重复攻击
//                    //从redis中获取当前nonce，如果不存在则允许通过，并在redis中存储当前随机数，并设置过期时间为10分钟，单用户区分
////					String save_nonce = Pub_Tools.getString_TrimZeroLenAsNull(redisTemplate.opsForValue().get(AccessKey+nonce));
//                    String save_nonce = Pub_Tools.getString_TrimZeroLenAsNull(redisTemplate.opsForValue().get(nonce));
//                    if(save_nonce==null){
////						redisTemplate.opsForValue().set(AccessKey+nonce, nonce,10,TimeUnit.MINUTES);
//                        redisTemplate.opsForValue().set(nonce, nonce,10, TimeUnit.MINUTES);
//                    }else{//如果存在，不允许继续请求。
//                        return false;
//                    }
//                }
//                /**
//                 * 上述验证通过，开始验证参数是否正确
//                 * 1、按照请求参数名的字母升序排列非空请求参数（包含AccessKey），使用URL键值对的格式（即key1=value1&key2=value2…）拼接成字符串stringA，这其中包含timestamp和nonce
//                 * 2、在stringA最后拼接上Secretkey得到字符串stringSignTemp
//                 * 3、对stringSignTemp进行MD5运算，并将得到的字符串所有字符转换为大写，得到sign值。
//                 * 4、比较计算出的sign值和用户传入的sign值是否相等
//                 */
//                //排序
//                Set<String> keySet = paramMap.keySet();
//                String[] keyArray = keySet.toArray(new String[keySet.size()]);
//                Arrays.sort(keyArray);
//                StringBuilder sb = new StringBuilder();
//                for (String k : keyArray) {
//                    if (k.equals("sign")) {
//                        continue;
//                    }
//                    if (paramMap.get(k).trim().length() > 0) // 参数值为空，则不参与签名
//                        sb.append(k).append("=").append(paramMap.get(k).trim()).append("&");
//                }
//                //暂时不需要个人认证
////		        sb.append("key=").append(SecretKey);
//                //加密
//                String server_sign = KeyUtil.MD5(sb.toString()).toUpperCase();
//                //判断当前签名是否正确
//                if(server_sign.equals(sign)){
//                    return true;
//                }
//            } catch (Exception e) {
//                return false;
//            }
//        }
//        return false;
//    }


	 /*
     */

    /**
     * 唯修汇外部接口签名验证
     *
     * @return
     *//*
     public static Boolean checkSign(HttpServletRequest request){
		 Boolean flag= false;
		 String appid = request.getParameter("appid");//appid
		 if(!appid.equals(appidOfWxh)){
			 throw  RestCommService.buildBadRequest("appid错误");
		 }
		 String sign = request.getParameter("sign");//签名
		 String timestamp = request.getParameter("timestamp");//时间戳
		//check时间戳的值是否在当前时间戳前后一小时以内
		String currTimestamp = String.valueOf(new Date().getTime() / 1000); // 当前时间的时间戳
		int currTimestampNum = Integer.parseInt(currTimestamp);
		int verifyTimestampNum = Integer.parseInt(timestamp); // 时间戳的数值
		// 在一小时范围之外，访问已过期
		if (Math.abs(verifyTimestampNum - currTimestampNum) > 600) {
			throw  RestCommService.buildBadRequest("sigin已经过期");
		}
		//检查sigin是否过期
		 Enumeration<?> pNames =  request.getParameterNames();
		 Map<String, String> params = new HashMap<String, String>();
		 while (pNames.hasMoreElements()) {  
		     String pName = (String) pNames.nextElement();  
		     if("sign".equals(pName)) continue;  
		     String pValue = (String)request.getParameter(pName);  
		     params.put(pName, pValue);  
		 }
		 if(sign.equals(getSign(params, secretKeyOfWxh))){
			 flag = true;
		 }
		 return flag;
	 }*/

    /**
     * 得到签名
     *
     * @param paramMap     参数集合不含clientSecret
     * @param clientSecret 验证接口的clientSecret
     * @return
     */
    public static String getSign(Map<String, String> paramMap, String clientSecret) {
        //排序
        Set<String> keySet = paramMap.keySet();
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        Arrays.sort(keyArray);
        StringBuilder sb = new StringBuilder();
        String signType = paramMap.get("signType");
        SignType type = null;
        if (StringUtils.isNotBlank(signType)) {
            type = SignType.valueOf(signType);
        }
        if (type == null) {
            type = SignType.MD5;
        }
        for (String k : keyArray) {
            if (k.equals("sign")) {
                continue;
            }
            if (paramMap.get(k).trim().length() > 0) {
                // 参数值为空，则不参与签名
                sb.append(k).append("=").append(paramMap.get(k).trim()).append("&");
            }
        }
        //暂时不需要个人认证
        sb.append("clientSecret=").append(clientSecret);
        String signStr = "";
        //加密
        switch (type) {
            case MD5:
                signStr = KeyUtils.md5Hex(sb.toString()).toLowerCase();
                break;
            case SHA1:
                signStr = KeyUtils.sha1Hex(sb.toString()).toLowerCase();
                break;
            case SHA256:
                signStr = KeyUtils.sha256Hex(sb.toString()).toLowerCase();
                break;
            default:
                break;
        }
        return signStr;
    }


    public enum SignType {
        MD5,
        SHA1,
        SHA256;

        public static boolean contains(String type) {
            for (SignType typeEnum : SignType.values()) {
                if (typeEnum.name().equals(type)) {
                    return true;
                }
            }
            return false;
        }
    }

}