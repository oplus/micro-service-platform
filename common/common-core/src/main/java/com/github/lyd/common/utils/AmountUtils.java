package com.github.lyd.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * @author: admin
 * @date: 2018/7/31 14:50
 * @description:
 */
public class AmountUtils {
    private static final Double MILLION = 10000.0;
    private static final Double MILLIONS = 1000000.0;
    private static final Double BILLION = 100000000.0;
    private static final String MILLION_UNIT = "万";
    private static final String BILLION_UNIT = "亿";
    private static final int DEFAULT_DECIMAL = 2;

    /**
     * 对数字进行四舍五入，保留2位小数
     *
     * @param number   要四舍五入的数字
     * @param decimal  保留的小数点数
     * @param rounding 是否四舍五入
     * @return
     * @author
     * @version 1.00.00
     * @date 2018年1月18日
     */
    public static Double formatNumber(double number, int decimal, boolean rounding) {
        BigDecimal bigDecimal = new BigDecimal(number);

        if (rounding) {
            return bigDecimal.setScale(decimal, RoundingMode.HALF_UP).doubleValue();
        } else {
            return bigDecimal.setScale(decimal, RoundingMode.DOWN).doubleValue();
        }
    }

    /**
     * 对四舍五入的数据进行补0显示，即显示.00
     *
     * @return
     * @author
     * @version 1.00.00
     * @date 2018年1月23日
     */
    public static String zeroFill(double number) {
        String value = String.valueOf(number);
        String str = ".";
        if (value.indexOf(str) < 0) {
            value = value + ".00";
        } else {
            String decimalValue = value.substring(value.indexOf(str) + 1);

            if (decimalValue.length() < DEFAULT_DECIMAL) {
                value = value + "0";
            }
        }
        return value;
    }

    /**
     * 将数字转换成以万为单位或者以亿为单位，因为在前端数字太大显示有问题
     *
     * @param amount 报销金额
     * @return 120.00
     * 1816.00亿
     * 122.21万
     * 1.29亿
     */
    public static String amountFormatCny(double amount) {
        //最终返回的结果值
        String result = String.valueOf(amount);
        //四舍五入后的值
        double value = 0;
        //转换后的值
        double tempValue = 0;
        //余数
        double remainder = 0;

        //金额大于1百万小于1亿
        if (amount > MILLIONS && amount < BILLION) {
            tempValue = amount / MILLION;
            remainder = amount % MILLION;

            //余数小于5000则不进行四舍五入
            if (remainder < (MILLION / DEFAULT_DECIMAL)) {
                value = formatNumber(tempValue, DEFAULT_DECIMAL, false);
            } else {
                value = formatNumber(tempValue, DEFAULT_DECIMAL, true);
            }
            //如果值刚好是10000万，则要变成1亿
            if (value == MILLION) {
                result = zeroFill(value / MILLION) + BILLION_UNIT;
            } else {
                result = zeroFill(value) + MILLION_UNIT;
            }
        }
        //金额大于1亿
        else if (amount > BILLION) {
            tempValue = amount / BILLION;
            remainder = amount % BILLION;

            //余数小于50000000则不进行四舍五入
            if (remainder < (BILLION / DEFAULT_DECIMAL)) {
                value = formatNumber(tempValue, DEFAULT_DECIMAL, false);
            } else {
                value = formatNumber(tempValue, DEFAULT_DECIMAL, true);
            }
            result = zeroFill(value) + BILLION_UNIT;
        } else {
            result = zeroFill(amount);
        }
        return result;
    }


    public static long parseAmount2Long(BigDecimal bigAmount) {
        if (bigAmount == null) {
            return 0L;
        } else {
            double amount = bigAmount.doubleValue();
            Double db = Double.valueOf(amount * 100.0D);
            DecimalFormat df = new DecimalFormat("#");
            String s = df.format(db);
            return Long.parseLong(s);
        }
    }

    public static long parseAmountStr2Long(String amountStr) {
        if (amountStr != null && !"".equals(amountStr)) {
            double amount = Double.parseDouble(amountStr);
            Double db = Double.valueOf(amount * 100.0D);
            DecimalFormat df = new DecimalFormat("#");
            String s = df.format(db);
            return Long.parseLong(s);
        } else {
            return 0L;
        }
    }

    public static Long yuan2Fen(Double yuan) {
        Double formatMoney = Double.valueOf(formatMoney(yuan.doubleValue() * 100.0D, 2));
        return Long.valueOf(formatMoney.longValue());
    }

    public static BigDecimal yuan2Fen(BigDecimal yuan) {
        return yuan.multiply(new BigDecimal(100));
    }

    public static String parseAmountLong2Str(Long amountLong) {
        if (amountLong == null) {
            return "0.00";
        } else {
            DecimalFormat df = new DecimalFormat("0.00");
            double d = (double) amountLong.longValue() / 100.0D;
            String s = df.format(d);
            return s;
        }
    }

    public static BigDecimal parseAmountLong2Big(Long amountLong) {
        if (amountLong == null) {
            return new BigDecimal("0.00");
        } else {
            DecimalFormat df = new DecimalFormat("0.00");
            double d = (double) amountLong.longValue() / 100.0D;
            String s = df.format(d);
            return new BigDecimal(s);
        }
    }

    public static String fen2YuanThousand(String amountFen) {
        if (amountFen == null) {
            return "0.00";
        } else {
            BigDecimal amountYuan = (new BigDecimal(amountFen)).divide(new BigDecimal(100));
            return AmountFormatThousand(amountYuan.toString(), 2);
        }
    }

    public static String fen2YuanL(BigDecimal amountFen) {
        if (amountFen == null) {
            return "0.00";
        } else {
            BigDecimal amountYuan = amountFen.divide(new BigDecimal(100));
            return AmountFormatThousand(amountYuan.toString(), 2);
        }
    }

    public static String fen2YuanThousand(Long amountFen) {
        if (amountFen == null) {
            return "0.00";
        } else {
            BigDecimal amountYuan = (new BigDecimal(amountFen.longValue())).divide(new BigDecimal(100));
            return AmountFormatThousand(amountYuan.toString(), 2);
        }
    }

    public static double formatMoney(double value, int wei) {
        return (new BigDecimal(String.valueOf(value))).setScale(wei, RoundingMode.HALF_UP).doubleValue();
    }

    public static BigDecimal formatMoney(BigDecimal amount, int wei) {
        return amount.setScale(wei, RoundingMode.HALF_UP);
    }

    public static Double fen2Yuan(Long fen) {
        Double yuan = Double.valueOf((double) fen.longValue() / 100.0D);
        BigDecimal big = new BigDecimal((double) fen.longValue() / 100.0D);
        yuan = Double.valueOf(big.setScale(2, 4).doubleValue());
        return yuan;
    }

    public static BigDecimal fen2Yuan(BigDecimal fen) {
        return fen.divide(new BigDecimal(100), 2, 4);
    }

    public static String amountFormat(String amountStr) {
        Long amountFen = Long.valueOf(parseAmountStr2Long(amountStr));
        BigDecimal amountFormat = new BigDecimal(parseAmountLong2Str(amountFen));
        return String.valueOf(amountFormat);
    }

    /**
     * 格式化为千分位
     *
     * @param amountStr
     * @return
     */
    public static String AmountFormatThousand(String amountStr) {
        return AmountFormatThousand(amountStr, 2);
    }

    /**
     * 格式化为千分位
     *
     * @param amountStr
     * @return
     */
    public static String numberFormatLi(String amountStr) {
        return AmountFormatThousand(amountStr, 0);
    }

    /**
     * 格式化为千分位
     *
     * @param amountStr
     * @return
     */
    public static String AmountFormatThousand(String amountStr, int num) {
        String format = "###,##0";
        boolean flag = true;

        for (int i = 0; i < num; ++i) {
            if (flag) {
                format = format + ".0";
                flag = false;
            } else {
                format = format + "0";
            }
        }

        DecimalFormat df = new DecimalFormat(format);
        return df.format(new BigDecimal(amountStr));
    }

    public static String format(BigDecimal big) {
        double yuan = big.setScale(1, 4).doubleValue();
        DecimalFormat df = new DecimalFormat("0.0");
        String s = df.format(yuan);
        return s;
    }

    /**
     * 测试方法入口
     *
     * @param args
     * @author
     * @version 1.00.00
     * @date 2018年1月18日
     */
    public static void main(String[] args) throws Exception {
        System.out.println(amountFormatCny(120));
        System.out.println(amountFormatCny(1200.35));
        System.out.println(amountFormatCny(12000.35));
        System.out.println(amountFormatCny(120000.35));
        System.out.println(amountFormatCny(1200000.35));
        System.out.println(amountFormatCny(12000000.35));
        System.out.println(amountFormatCny(120000000.35));
        System.out.println(amountFormatCny(1200000000.35));
    }
}
