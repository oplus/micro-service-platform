package com.github.lyd.common.exception.handler;

import com.github.lyd.common.constants.HttpCode;
import com.github.lyd.common.model.dto.ResponseData;
import com.github.lyd.common.utils.WebUtils;
import jdk.nashorn.internal.runtime.logging.Logger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义未认证处理
 *
 * @author liuyadu
 */
@Slf4j
public class GoAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException exception) throws IOException, ServletException {
        log.error(exception.getMessage());
    }
}