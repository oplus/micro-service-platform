package com.github.lyd.common.config;

import com.github.lyd.common.utils.SignUtils;
import com.github.lyd.common.utils.WebUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.boot.autoconfigure.security.oauth2.OAuth2ClientProperties;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 微服务之间feign调用请求头丢失的问题
 * 实现RequestInterceptor拦截器，在其中将请求头中的token信息加入到被调用微服务的请求头中
 * 加入sign数字签名
 *
 * @author liuyadu
 */
public class FeignRequestConfig implements RequestInterceptor {

    public static String TOKEN_HEADER = "authorization";
    public static String X_REQUEST_SID = "X-Request-SID";

    private IdWorker idWorker;

    private final static String CLIENT_KEY = "gateway";
    private final static String CLIENT_SECRET = "123456";

    public FeignRequestConfig(OAuth2ClientProperties oAuth2ClientProperties, IdWorker idWorker) {
        this.idWorker = idWorker;
    }

    @Override
    public void apply(RequestTemplate template) {
        // 方式oauth请求头丢失
        template.header(TOKEN_HEADER, getHeaders(getHttpServletRequest()).get(TOKEN_HEADER));
        // 加入请求序列号
        if (idWorker != null) {
            template.header(X_REQUEST_SID, String.valueOf(idWorker.nextId()));
        }
        //加入签名
        HttpServletRequest request = getHttpServletRequest();
        Map paramsMap = WebUtils.getParameterMap(request);
        paramsMap.put("clientId", CLIENT_KEY);
        String sign = SignUtils.getSign(paramsMap, CLIENT_SECRET);
        template.query("sign", sign);
    }

    private HttpServletRequest getHttpServletRequest() {
        try {
            return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        } catch (Exception e) {
            return null;
        }
    }

    private Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> map = new LinkedHashMap<>();
        Enumeration<String> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }

}