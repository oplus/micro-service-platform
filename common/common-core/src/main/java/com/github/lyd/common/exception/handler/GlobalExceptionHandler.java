/*
 * MIT License
 *
 * Copyright (c) 2018 yadu.liu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package com.github.lyd.common.exception.handler;

import com.github.lyd.common.constants.HttpCode;
import com.github.lyd.common.model.dto.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.*;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 统一异常管理
 *
 * @author LYD
 * @date 2017/7/3
 */
@ControllerAdvice
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 统一异常处理
     *
     * @param ex
     * @param request
     * @param response
     * @return
     */
    @ExceptionHandler({Exception.class})
    public ResponseData exception(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        return resolveException(ex, request, response);
    }


    /**
     * 解析异常
     *
     * @param ex
     * @param request
     * @param response
     * @return
     */
    public static ResponseData resolveException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        String exception = ex.getMessage();
        log.error("错误解析:", ex.getMessage());
        String path = request.getRequestURI();
        String method = request.getMethod();
        HttpCode resultCode = HttpCode.ERROR;
        String message = "";
        if (ex instanceof HttpMessageNotReadableException || ex instanceof TypeMismatchException || ex instanceof MissingServletRequestParameterException) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resultCode = HttpCode.BAD_REQUEST;
        } else if (ex instanceof NoHandlerFoundException) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            resultCode = HttpCode.NOT_FOUND;
        } else if (ex instanceof HttpRequestMethodNotSupportedException) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resultCode = HttpCode.METHOD_NOT_ALLOWED;
        } else if (ex instanceof HttpMediaTypeNotAcceptableException) {
            resultCode = HttpCode.MEDIA_TYPE_NOT_ACCEPTABLE;
        } else if (ex instanceof MethodArgumentNotValidException) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            MethodArgumentNotValidException c = (MethodArgumentNotValidException) ex;
            List<ObjectError> errors = c.getBindingResult().getAllErrors();
            StringBuffer errorMsg = new StringBuffer();
            errors.stream().forEach(x -> errorMsg.append(x.getDefaultMessage()).append(";"));
            message = errorMsg.toString();
        }
        /**
         UsernameNotFoundException 用户找不到
         BadCredentialsException 坏的凭据
         AccountExpiredException 账户过期
         LockedException账户锁定
         DisabledException 账户不可用
         CredentialsExpiredException 证书过期
         */
        else if (ex instanceof UsernameNotFoundException) {
            resultCode = HttpCode.USER_NOT_EXIST;
        } else if (ex instanceof BadCredentialsException) {
            resultCode = HttpCode.USER_LOGIN_FAIL;
        } else if (ex instanceof AccountExpiredException) {
            resultCode = HttpCode.USER_ACCOUNT_EXPIRED;
        } else if (ex instanceof LockedException) {
            resultCode = HttpCode.USER_LOCKED;
        } else if (ex instanceof DisabledException) {
            resultCode = HttpCode.USER_DISABLED;
        } else if (ex instanceof CredentialsExpiredException) {
            resultCode = HttpCode.CREDENTIALS_EXPIRED;
        }
        /**
         * OAuth2Exception
         */
        else if (ex instanceof OAuth2Exception) {
            OAuth2Exception oAuth2Exception = (OAuth2Exception) ex;
            resultCode = HttpCode.getHttpCode(oAuth2Exception.getOAuth2ErrorCode());
        } else {
            message = resultCode.getMessage();
            exception = ex.getMessage();
        }
        request.setAttribute("message", message);
        request.setAttribute("status", resultCode.getCode());
        request.setAttribute("exception", exception);
        request.setAttribute("url", request.getRequestURL());
        return ResponseData.failed(resultCode.getCode(), message);
    }
}
