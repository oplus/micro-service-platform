/*
 * MIT License
 *
 * Copyright (c) 2018 yadu.liu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package com.github.lyd.common.constants;

import javax.persistence.EnumType;

/**
 * 返回码
 * @author admin
 */

public enum HttpCode {
    /**
     * 系统返回码
     */
    OK(0, "success"),
    BAD_REQUEST(400, "bad_request"),
    NOT_FOUND(404, "not_found"),

    METHOD_NOT_ALLOWED(405, "method_not_allowed"),
    MEDIA_TYPE_NOT_ACCEPTABLE(406, "media_type_not_acceptable"),

    /**
     * oauth2返回码
     */
    INVALID_TOKEN(21321, "invalid_token"),
    INVALID_SCOPE(21322, "invalid_scope"),
    INVALID_REQUEST(21323, "invalid_request"),
    INVALID_CLIENT(21324, "invalid_client"),
    INVALID_GRANT(21325, "invalid_grant"),
    REDIRECT_URI_MISMATCH(21326, "redirect_uri_mismatch"),
    UNAUTHORIZED_CLIENT(21327, "unauthorized_client"),
    EXPIRED_TOKEN(21328, "expired_token"),
    UNSUPPORTED_GRANT_TYPE(21329, "unsupported_grant_type"),
    UNSUPPORTED_RESPONSE_TYPE(21330, "unsupported_response_type"),
    ACCESS_DENIED(21332, "access_denied"),
    TEMPORARILY_UNAVAILABLE(21333, "temporarily_unavailable"),

    /**
     * 自定义返回码
     */
    SERVICE_NOT_FOUND(40004, "service_not_found"),
    USER_LOGIN_FAIL(41001, "user_login_fail"),
    USER_NOT_EXIST(41002, "user_not_exist"),
    USER_DISABLED(41003, "user_disabled"),
    USER_ACCOUNT_EXPIRED(41004, "user_account_expired"),
    CREDENTIALS_EXPIRED(41005, "credentials_expired"),
    USER_LOCKED(41004, "user_locked"),
    ERROR(50001, "error");


    private int code;
    private String message;

    HttpCode() {
    }

    private HttpCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static HttpCode getHttpCode(int val) {
        for (HttpCode type : HttpCode.values()) {
            if (type.getCode() == val) {
                return type;
            }
        }
        return ERROR;
    }

    public static HttpCode getHttpCode(String val) {
        for (HttpCode type : HttpCode.values()) {
            if (type.getMessage().equals(val)) {
                return type;
            }
        }
        return ERROR;
    }


    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


}
