/*
 * MIT License
 *
 * Copyright (c) 2018 yadu.liu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package com.github.lyd.common.model.vo;

import com.github.lyd.common.utils.StringUtils;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.util.Map;

/**
 * 查询对象
 *
 * @author liuyau
 * @date 2018/07/10
 */
public class Query implements Serializable {
    private static final long serialVersionUID = -1710273706052960025L;
    private static final int MIN_PAGE = 0;
    private static final int MAX_SIZE = 1000;
    private int page = 1;
    private int size = 10;
    private String sort;
    private String order;

    /**
     * 查询条件
     */
    private Map<String, Object> condition;
    /**
     * 排序
     */
    private String orderBy;

    public Query() {
    }

    public Query(Map<String, Object> condition) {
        this.condition = condition;
    }

    public Query(int page, int size, String sort, String order, Map<String, Object> condition) {
        if (page <= MIN_PAGE) {
            page = 1;
        }
        if (size > MAX_SIZE) {
            size = MAX_SIZE;
        }
        if (StringUtils.isBlank(order)) {
            order = "asc";
        }
        if (StringUtils.isNotBlank(sort)) {
            this.setOrderBy(String.format("%s %s", StringUtils.camelToUnderline(sort), order));
        }
        this.condition = condition;
    }

    /**
     * 获取查询参数,map
     *
     * @return
     */
    public Map<String, Object> getCondition() {
        return condition;
    }

    /**
     * 获取查询参数,对象
     *
     * @param beanClazz
     * @param <T>
     * @return
     */

    public <T> T getCondition(Class<T> beanClazz) {
        try {
            T bean = beanClazz.newInstance();
            BeanUtils.populate(bean, this.condition);
            return bean;
        } catch (Exception e) {
            return null;
        }
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setCondition(Map<String, Object> condition) {
        this.condition = condition;
    }
}
