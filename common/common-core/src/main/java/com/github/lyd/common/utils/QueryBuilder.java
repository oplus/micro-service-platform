/*
 * MIT License
 *
 * Copyright (c) 2018 yadu.liu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package com.github.lyd.common.utils;

import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;

/**
 * 查询条件构建
 * 适用于tkmapper 通用mapper
 *
 * @author admin
 */
public class QueryBuilder {
    /**
     * 定义examle对象，用于返回
     */
    private final Example example;

    public QueryBuilder(Class<?> tClass) {
        this.example = new Example(tClass);
    }//构造方法中传入Class参数，实例化example

    public QueryBuilder and(String columnName, OP op, Object value) {  //and
        Example.Criteria criteria = example.createCriteria();
        assembleParams(criteria, columnName, op, value);
        example.and(criteria);
        return this;
    }

    public QueryBuilder or(String columnName, OP op, Object value) { //or
        Example.Criteria criteria = example.createCriteria();
        assembleParams(criteria, columnName, op, value);
        //or 需要example调用or(Example.Criteria criteria) 方法
        example.or(criteria);
        return this;
    }

    private void assembleParams(Example.Criteria criteria, String columnName, OP op, Object value) {  //组装参数
        switch (op) {
            case LIKE:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andLike(columnName, "%" + value + "%");
                }
                break;
            case LEFT_LIKE:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andLike(columnName, "%" + value);
                }
                break;
            case LIKE_RIGHT:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andLike(columnName, value + "%");
                }
                break;
            case EQ:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andEqualTo(columnName, value);
                }
                break;
            case NE:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andNotEqualTo(columnName, value);
                }
                break;
            case GT:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andGreaterThan(columnName, value);
                }
                break;
            case NL:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andGreaterThanOrEqualTo(columnName, value);
                }
                break;
            case LT:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andLessThan(columnName, value);
                }
                break;
            case NG:
                if (!StringUtils.isEmpty(value)) {
                    criteria.andLessThanOrEqualTo(columnName, value);
                }
                break;
            case NULL:
                criteria.andIsNull(columnName);
                break;
            case NOTNULL:
                criteria.andIsNotNull(columnName);
                break;
            case IN:
                if (!StringUtils.isEmpty(value)) {
                    if (value instanceof ArrayList) {
                        criteria.andIn(columnName, (ArrayList) value);
                    }
                }
                break;
            case BETWEEN:
                if (!StringUtils.isEmpty(value)) {
                    if (value instanceof ArrayList) {
                        criteria.andBetween(columnName, ((ArrayList) value).get(0),
                                ((ArrayList) value).get(1));
                    }
                }
                break;
            case NOT_BETWEEN:
                if (!StringUtils.isEmpty(value)) {
                    if (value instanceof ArrayList) {
                        criteria.andNotBetween(columnName, ((ArrayList) value).get(0),
                                ((ArrayList) value).get(1));
                    }
                }
                break;
            case NOT_IN:
                if (!StringUtils.isEmpty(value)) {
                    if (value instanceof ArrayList) {
                        criteria.andNotIn(columnName, (ArrayList) value);
                    }
                }
                break;
            default:
        }

    }

    public Example build() {  //返回example实例
        return example;
    }

    public enum OP {
        LIKE, LEFT_LIKE, LIKE_RIGHT, EQ, NE, GT, NL, LT, NG, NULL, NOTNULL, IN, BETWEEN, NOT_BETWEEN, NOT_IN
    }

}
