/*
 * MIT License
 *
 * Copyright (c) 2018 yadu.liu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package com.github.lyd.common.model.dto;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.lyd.common.constants.HttpCode;
import com.github.lyd.common.utils.ApplicationUtils;
import com.google.common.collect.Maps;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

/**
 * @author admin
 */
public class ResponseData<T> implements Serializable {
    private static final long serialVersionUID = -6190689122701100762L;
    /**
     * 国际化配置
     */
    private MessageSource messageSource;
    private Locale locale = LocaleContextHolder.getLocale();
    /**
     * 消息码
     */
    private int code = 0;

    /**
     * 返回消息
     */
    private String message = "";

    /**
     * 返回数据
     */
    private T data;

    /**
     * 附加数据
     */
    private Map<String, Object> extra;

    /**
     * 服务器时间
     */
    private long timestamp = System.currentTimeMillis();

    @JSONField(serialize = false, deserialize = false)
    @JsonIgnore
    public boolean isOk() {
        return this.code == 0;
    }

    public ResponseData() {
        super();
    }

    public static <T> ResponseData ok() {
        return new ResponseData().setMessage("success");
    }
    public static <T> ResponseData ok(T data) {
        return new ResponseData().setData(data).setMessage("success");
    }

    public static <T> ResponseData ok(String msg, T result) {
        return new ResponseData().setMessage(msg).setData(result);
    }

    public static ResponseData failed(String msg) {
        return new ResponseData().setCode(HttpCode.ERROR.getCode()).setMessage(msg);
    }

    public static ResponseData failed() {
        return new ResponseData().setCode(HttpCode.ERROR.getCode()).setMessage(HttpCode.ERROR.getMessage());
    }

    public static ResponseData failed(Integer code, String msg) {
        return new ResponseData().setCode(code).setMessage(msg);
    }

    public int getCode() {
        return code;
    }

    public ResponseData setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseData setMessage(String message) {
        try {
            HttpCode httpCode = HttpCode.getHttpCode(this.code);
            if (httpCode != null) {
                if (messageSource == null) {
                    messageSource = ApplicationUtils.getBean(MessageSource.class);
                }
                // 读取国际化消息
                message = messageSource.getMessage(httpCode.getMessage(), null, message, locale);
            }
        } catch (Exception e) {

        }
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public ResponseData setData(T data) {
        this.data = data;
        return this;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public ResponseData setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public Map<String, Object> getExtra() {
        return extra;
    }

    public ResponseData setExtra(Map<String, Object> extra) {
        this.extra = extra;
        return this;
    }

    public ResponseData putExtra(String key, Object value) {
        if (this.extra == null) {
            this.extra = Maps.newHashMap();
        }
        this.extra.put(key, value);
        return this;
    }

}
