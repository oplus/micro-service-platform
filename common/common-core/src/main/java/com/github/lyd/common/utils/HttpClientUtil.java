/*
 * MIT License
 *
 * Copyright (c) 2018 yadu.liu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package com.github.lyd.common.utils;

import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.util.*;
import java.util.Map.Entry;

/**
 * HttpClient工具类
 */
public class HttpClientUtil {

    public static final int CONN_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    public static final String CHARSET = "UTF-8";

    public static final int CACHE = 10 * 1024;
    public static boolean IS_WINDOWS = false;
    public static String SPLASH = "";
    public static String ROOT = "";

    private static HttpClient client = null;

    static {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(128);
        cm.setDefaultMaxPerRoute(128);
        client = HttpClients.custom().setConnectionManager(cm).setConnectionManagerShared(true).build();
        String osName = "os.name";
        String windows = "windows";
        if (System.getProperty(osName) != null && System.getProperty(osName).toLowerCase().contains(windows)) {
            IS_WINDOWS = true;
            SPLASH = "\\";
            ROOT = "D:";
        } else {
            IS_WINDOWS = false;
            SPLASH = "/";
            ROOT = "/search";
        }
    }

    public static String post(String url, String parameterStr) throws Exception {
        return post(url, parameterStr, "application/x-www-form-urlencoded", CHARSET, CONN_TIMEOUT, READ_TIMEOUT);
    }

    public static String post(String url, String parameterStr, String CHARSET, Integer CONN_TIMEOUT, Integer READ_TIMEOUT) throws Exception {
        return post(url, parameterStr, "application/x-www-form-urlencoded", CHARSET, CONN_TIMEOUT, READ_TIMEOUT);
    }

    public static String post(String url, Map params) throws
            Exception {
        return post(url, params, null, CONN_TIMEOUT, READ_TIMEOUT);
    }

    public static String post(String url, Map params, Map headers) throws
            Exception {
        return post(url, params, headers, CONN_TIMEOUT, READ_TIMEOUT);
    }

    public static String post(String url, Map params, Integer CONN_TIMEOUT, Integer READ_TIMEOUT) throws
            Exception {
        return post(url, params, null, CONN_TIMEOUT, READ_TIMEOUT);
    }

    public static String get(String url) throws Exception {
        return get(url, CHARSET, null, null);
    }

    public static String get(String url, String CHARSET) throws Exception {
        return get(url, CHARSET, CONN_TIMEOUT, READ_TIMEOUT);
    }

    public static String get(String url, Map parameters) throws Exception {
        StringBuffer params = new StringBuffer();
        for (Iterator iter = parameters.entrySet().iterator(); iter.hasNext(); ) {
            Entry element = (Entry) iter.next();
            params.append(element.getKey().toString());
            params.append("=");
            params.append(EncodeUtils.urlEncode(String.valueOf(element.getValue())));
            params.append("&");
        }
        if (params.length() > 0) {
            params = params.deleteCharAt(params.length() - 1);
        }
        url += "?" + params.toString();
        return get(url);
    }


    /**
     * 发送一个 Post 请求, 使用指定的字符集编码.
     *
     * @param url
     * @param body         RequestBody
     * @param mimeType     例如 application/xml "application/x-www-form-urlencoded" dto=1&b=2&c=3
     * @param CHARSET      编码
     * @param CONN_TIMEOUT 建立链接超时时间,毫秒.
     * @param READ_TIMEOUT 响应超时时间,毫秒.
     * @return ResponseBody, 使用指定的字符集编码.
     * @throws ConnectTimeoutException 建立链接超时异常
     * @throws SocketTimeoutException  响应超时
     * @throws Exception
     */
    public static String post(String url, String body, String mimeType, String CHARSET, Integer CONN_TIMEOUT, Integer READ_TIMEOUT)
            throws Exception {
        HttpClient client = null;
        HttpPost post = new HttpPost(url.trim());
        String result = "";
        try {
            if (StringUtils.isNotBlank(body)) {
                HttpEntity entity = new StringEntity(body, ContentType.create(mimeType, CHARSET));
                post.setEntity(entity);
            }
            // 设置参数
            Builder customReqConf = RequestConfig.custom();
            if (CONN_TIMEOUT != null) {
                customReqConf.setConnectTimeout(CONN_TIMEOUT);
            }
            if (READ_TIMEOUT != null) {
                customReqConf.setSocketTimeout(READ_TIMEOUT);
            }
            post.setConfig(customReqConf.build());
            client = HttpClientUtil.client;
            HttpResponse res = client.execute(post);
            result = IOUtils.toString(res.getEntity().getContent(), CHARSET);
        } finally {
            post.releaseConnection();
            if (client != null && client instanceof CloseableHttpClient) {
                ((CloseableHttpClient) client).close();
            }
        }
        return result;
    }


    /**
     * 提交form表单
     *
     * @param url
     * @param params
     * @param CONN_TIMEOUT
     * @param READ_TIMEOUT
     * @return
     * @throws ConnectTimeoutException
     * @throws SocketTimeoutException
     * @throws Exception
     */
    public static String post(String url, Map<String, Object> params, Map<String, Object> headers, Integer CONN_TIMEOUT, Integer READ_TIMEOUT) throws
            Exception {

        HttpClient client = null;
        HttpPost post = new HttpPost(url.trim());
        try {
            if (params != null && !params.isEmpty()) {
                List<NameValuePair> formParams = new ArrayList<NameValuePair>();
                Set<Entry<String, Object>> entrySet = params.entrySet();
                for (Entry<String, Object> entry : entrySet) {
                    formParams.add(new BasicNameValuePair(entry.getKey(), String.valueOf(entry.getValue())));
                }
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);
                post.setEntity(entity);
            }
            if (headers != null && !headers.isEmpty()) {
                for (Entry<String, Object> entry : headers.entrySet()) {
                    post.addHeader(entry.getKey(), String.valueOf(entry.getValue()));
                }
            }
            // 设置参数  
            Builder customReqConf = RequestConfig.custom();
            if (CONN_TIMEOUT != null) {
                customReqConf.setConnectTimeout(CONN_TIMEOUT);
            }
            if (READ_TIMEOUT != null) {
                customReqConf.setSocketTimeout(READ_TIMEOUT);
            }
            post.setConfig(customReqConf.build());
            client = HttpClientUtil.client;
            HttpResponse res = client.execute(post);
            return IOUtils.toString(res.getEntity().getContent(), CHARSET);
        } finally {
            post.releaseConnection();
            if (client != null && client instanceof CloseableHttpClient) {
                ((CloseableHttpClient) client).close();
            }
        }
    }


    /**
     * 发送一个 GET 请求
     *
     * @param url
     * @param CHARSET
     * @param CONN_TIMEOUT 建立链接超时时间,毫秒.
     * @param READ_TIMEOUT 响应超时时间,毫秒.
     * @return
     * @throws ConnectTimeoutException 建立链接超时
     * @throws SocketTimeoutException  响应超时
     * @throws Exception
     */
    public static String get(String url, String CHARSET, Integer CONN_TIMEOUT, Integer READ_TIMEOUT)
            throws Exception {

        HttpClient client = null;
        HttpGet get = new HttpGet(url.trim());
        get.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
        String result = "";
        try {
            // 设置参数
            Builder customReqConf = RequestConfig.custom();
            if (CONN_TIMEOUT != null) {
                customReqConf.setConnectTimeout(CONN_TIMEOUT);
            }
            if (READ_TIMEOUT != null) {
                customReqConf.setSocketTimeout(READ_TIMEOUT);
            }
            get.setConfig(customReqConf.build());
            client = HttpClientUtil.client;
            HttpResponse res = client.execute(get);
            result = IOUtils.toString(res.getEntity().getContent(), CHARSET);
        } finally {
            get.releaseConnection();
            if (client != null && client instanceof CloseableHttpClient) {
                ((CloseableHttpClient) client).close();
            }
        }
        return result;
    }


    /**
     * 从 response 里获取 CHARSET
     *
     * @param ressponse
     * @return
     */
    @SuppressWarnings("unused")
    private static String getCharsetFromResponse(HttpResponse ressponse) {
        // Content-Type:text/html; CHARSET=GBK  
        if (ressponse.getEntity() != null && ressponse.getEntity().getContentType() != null && ressponse.getEntity().getContentType().getValue() != null) {
            String contentType = ressponse.getEntity().getContentType().getValue();
            if (contentType.contains("CHARSET=")) {
                return contentType.substring(contentType.indexOf("CHARSET=") + 8);
            }
        }
        return null;
    }


    /**
     * 多文件上传文件
     *
     * @param url
     * @param params
     * @param CONN_TIMEOUT
     * @param READ_TIMEOUT
     * @return
     * @throws ConnectTimeoutException
     * @throws SocketTimeoutException
     * @throws Exception
     */
    public static String postFiles(String url, Map<String, Object> params, Map<String, Object> headers, Integer CONN_TIMEOUT, Integer READ_TIMEOUT, String name, File[] files) throws
            Exception {

        HttpClient client = null;
        HttpPost post = new HttpPost(url.trim());
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        try {
            if (headers != null && !headers.isEmpty()) {
                for (Entry<String, Object> entry : headers.entrySet()) {
                    post.addHeader(entry.getKey(), String.valueOf(entry.getValue()));
                }
            }
            if (params != null && !params.isEmpty()) {
                Set<Entry<String, Object>> entrySet = params.entrySet();
                for (Entry<String, Object> entry : entrySet) {
                    builder.addPart(entry.getKey(), new StringBody(String.valueOf(entry.getValue()), ContentType.create("text/plain", Consts.UTF_8)));
                }
            }
            if (files != null && files.length > 0) {
                for (File file : files) {
                    builder.addBinaryBody(name, file);
                }
            }
            // 设置参数  
            Builder customReqConf = RequestConfig.custom();
            if (CONN_TIMEOUT != null) {
                customReqConf.setConnectTimeout(CONN_TIMEOUT);
            }
            if (READ_TIMEOUT != null) {
                customReqConf.setSocketTimeout(READ_TIMEOUT);
            }
            HttpEntity entity = builder.build();
            post.setEntity(entity);
            client = HttpClientUtil.client;
            HttpResponse res = client.execute(post);
            return IOUtils.toString(res.getEntity().getContent(), "UTF-8");
        } finally {
            post.releaseConnection();
            if (client != null && client instanceof CloseableHttpClient) {
                ((CloseableHttpClient) client).close();
            }
        }
    }


    /**
     * 根据url下载文件，文件名从response header头中获取
     *
     * @param url
     * @return
     */
    public static String download(String url) {
        return download(url, null);
    }

    /**
     * 根据url下载文件，保存到filepath中
     *
     * @param url
     * @param filepath
     * @return
     */
    public static String download(String url, String filepath) {
        try {
            HttpGet httpget = new HttpGet(url);
            HttpResponse response = client.execute(httpget);

            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent();
            if (filepath == null) {
                filepath = getFilePath(response);
            }
            File file = new File(filepath);
            file.getParentFile().mkdirs();
            FileOutputStream fileout = new FileOutputStream(file);
            /**
             * 根据实际运行效果 设置缓冲区大小
             */
            byte[] buffer = new byte[CACHE];
            int ch = 0;
            while ((ch = is.read(buffer)) != -1) {
                fileout.write(buffer, 0, ch);
            }
            is.close();
            fileout.flush();
            fileout.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取response要下载的文件的默认路径
     *
     * @param response
     * @return
     */
    public static String getFilePath(HttpResponse response) {
        String filepath = ROOT + SPLASH;
        String filename = getFileName(response);

        if (filename != null) {
            filepath += filename;
        } else {
            filepath += getRandomFileName();
        }
        return filepath;
    }

    /**
     * 获取response header中Content-Disposition中的filename值
     *
     * @param response
     * @return
     */
    public static String getFileName(HttpResponse response) {
        Header contentHeader = response.getFirstHeader("Content-Disposition");
        String filename = null;
        if (contentHeader != null) {
            HeaderElement[] values = contentHeader.getElements();
            if (values.length == 1) {
                NameValuePair param = values[0].getParameterByName("filename");
                if (param != null) {
                    try {
                        filename = param.getValue();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return filename;
    }

    /**
     * 获取随机文件名
     *
     * @return
     */
    public static String getRandomFileName() {
        return String.valueOf(System.currentTimeMillis());
    }

    /**
     * 获取response header
     *
     * @param response
     */
    public static void outHeaders(HttpResponse response) {
        Header[] headers = response.getAllHeaders();
        for (int i = 0; i < headers.length; i++) {
            System.out.println(headers[i]);
        }
    }
}
