/*
 * MIT License
 *
 * Copyright (c) 2018 yadu.liu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.github.lyd.common.config;

import com.github.lyd.common.exception.handler.GlobalExceptionHandler;
import com.github.lyd.common.utils.ApplicationUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.security.oauth2.OAuth2ClientProperties;
import org.springframework.boot.autoconfigure.security.oauth2.authserver.AuthorizationServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

/**
 * 自动配置
 *
 * @author admin
 */
@Slf4j
@Configuration
public class GlobalAutoConfig {
    @Autowired
    private OAuth2ClientProperties oAuth2ClientProperties;
    @Qualifier("authServerProp")
    @Autowired
    private AuthorizationServerProperties authorizationServerProperties;
    @Autowired
    private DefaultAccessTokenConverter defaultAccessTokenConverter;

    @Autowired
    private IdWorker idWorker;

    @Bean(name = "authServerProp")
    public AuthorizationServerProperties authorizationServerProperties() {
        return new AuthorizationServerProperties();
    }

    /**
     * Spring上下文工具配置
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(ApplicationUtils.class)
    public ApplicationUtils applicationUtil() {
        ApplicationUtils applicationUtil = new ApplicationUtils();
        log.info("=============自动配置 ApplicationUtil=============");
        return applicationUtil;
    }

    /**
     * 统一异常处理配置
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(GlobalExceptionHandler.class)
    public GlobalExceptionHandler globalExceptionHandler() {
        GlobalExceptionHandler exceptionHandler = new GlobalExceptionHandler();
        log.info("=============自动配置 GlobalExceptionHandler=============");
        return exceptionHandler;
    }

    /**
     * FeignClient请求配置
     *
     * @FeignClient(value = "rbac",configuration = FeignRequestConfig.class)
     */
    @Bean
    @ConditionalOnMissingBean(FeignRequestConfig.class)
    FeignRequestConfig feignRequestConfig() {
        FeignRequestConfig feignRequestConfig = new FeignRequestConfig(oAuth2ClientProperties, idWorker);
        log.info("=============自动配置 FeignRequestConfig=============");
        return feignRequestConfig;
    }


    /**
     * Oauth单点登录
     * 资源服务器-远程检查token配置
     *
     * @return
     */
    @Primary
    @Bean
    @ConditionalOnMissingBean(RemoteTokenServices.class)
    public RemoteTokenServices remoteTokenServices() {
        log.info("=============自动配置 RemoteTokenServices=============");
        AuthorizationServerProperties serverProperties = authorizationServerProperties();
        RemoteTokenServices remoteTokenServices = new RemoteTokenServices();
        remoteTokenServices.setCheckTokenEndpointUrl(serverProperties.getCheckTokenAccess());
        remoteTokenServices.setClientId(oAuth2ClientProperties.getClientId());
        remoteTokenServices.setClientSecret(oAuth2ClientProperties.getClientSecret());
        remoteTokenServices.setAccessTokenConverter(defaultAccessTokenConverter);
        log.info("clientId={},clientSecret ={},checkTokenAccess={}", oAuth2ClientProperties.getClientId(), oAuth2ClientProperties.getClientSecret(), serverProperties.getCheckTokenAccess());
        return remoteTokenServices;
    }

    /**
     * 默认token转换器
     *
     * @return
     */
    @Bean
    @Primary
    @ConditionalOnMissingBean(DefaultAccessTokenConverter.class)
    public DefaultAccessTokenConverter defaultAccessTokenConverter() {
        return new DefaultAccessTokenConverter();
    }

}