package com.github.lyd.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.net.InetAddress;
import java.text.NumberFormat;
import java.util.Properties;

@Controller
public class IndexController {

    @GetMapping(value = {"", "/index"})
    public String index() {
        return "index";
    }

    @GetMapping("welcome")
    public String welcome(HttpServletRequest request,Model model) {
        ServletContext context =   request.getServletContext();
        String serverInfo = context.getServerInfo();
        Runtime runtime = Runtime.getRuntime();
        Properties props = System.getProperties();
        Map<String, Object> map = Maps.newHashMap();
        double freeMemory = (double)runtime.freeMemory()/(1024*1024);
        double totalMemory = (double)runtime.totalMemory()/(1024*1024);
        double usedMemory = totalMemory - freeMemory;
        double percentFree = (usedMemory/totalMemory)*100.0;
        NumberFormat format = NumberFormat.getPercentInstance();
        format.setMaximumIntegerDigits(1);//设置保留几位小数
        String percentFreeFmt = format.format(percentFree);

        //JVM可以使用的总内存
        map.put("totalMemory",totalMemory);
        //JVM可以使用的剩余内存
        map.put("freeMemory",freeMemory);
        map.put("usedMemory",usedMemory);
        map.put("percentFree",percentFree);
        map.put("percentFreeFmt",percentFreeFmt);
        //Java的运行环境版本
        map.put("javaVersion",props.getProperty("java.version"));
        map.put("serverInfo",serverInfo);
        map.put("serverDate",new Date());
        map.put("domain",request.getServerName());
        map.put("osName",props.getProperty("os.name"));
        map.put("osArch",props.getProperty("os.arch"));
        map.put("osVersion",props.getProperty("os.version"));
        try {
            InetAddress addr = InetAddress.getLocalHost();
            String ip = addr.getHostAddress();
            map.put("ip",ip);
        } catch (UnknownHostException e) {
            map.put("ip","127.0.0.1");
        }
        model.addAllAttributes(map);
        return "welcome";
    }

}